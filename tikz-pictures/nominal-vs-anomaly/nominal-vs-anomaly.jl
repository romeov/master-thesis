using Plots
using Distributions
using PGFPlotsX

xs = LinRange(0, 1, 100);
Plot(Coordinates(xs, pdf.(Beta(), xs)))
