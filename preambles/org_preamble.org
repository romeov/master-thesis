#+LATEX_CLASS: report
#+LATEX_CLASS_OPTIONS: [a4paper, 11pt,hidelinks,oneside,svgnames]
#+LATEX_COMPILER: lualatex
#+LATEX_HEADER: \usepackage{bbm, svg, stmaryrd}
#+LATEX_HEADER_EXTRA: \input{preambles/tex_preamble}
#+OPTIONS: toc:nil
#+OPTIONS: H:6
#+OPTIONS: todo:nil
#+OPTIONS: broken-links:mark
#+PROPERTY: header-args :eval never-export

\dept{Computer Science}
\principaladvisor{Andreas Krause}
\coprincipaladvisor{Mykel Kochenderfer}
\firstreader{Jonas Rothfuss}
# \secondreader{Sydney Katz}
\submitdate{August 2022}
\copyrightfalse
\tablespagefalse
\beforepreface
